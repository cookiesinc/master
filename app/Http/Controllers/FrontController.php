<?php

namespace App\Http\Controllers;

use App\Comment;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class FrontController extends Controller
{

    //Main mage view
    public function index()
    {
        $users = User::orderBy('name', 'asc')->get();

        return view('welcome',compact('users'));
    }

    public function search() {

        $keyword = Input::get('keyword');
        $sort = Input::get('sort');
        $reverse = Input::get('reverse');

        if($keyword != '') {

            $data = User::search($keyword,$sort,$reverse);

            return $data;

        } else {

            $keyword = '';

            $data = User::search($keyword,$sort,$reverse);

            return $data;

        }
    }

}
