<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    public  function comments()
    {
        return $this->hasMany('App\Comment');
    }

    public static function getLastComment($id)
    {
        $comments = User::find($id)->comments()->orderby('created_at', 'desc')->first();

        return $comments;
    }

    public static function getCountComments($id)
    {
        $rating = User::find($id)->comments;

        return count($rating);
    }


    public static function search($keyword,$sort,$reverse)
    {
        if($sort == 'created_at'){
            $value = $sort;
        }else{
            $value = function($user) {return $user->comments->count();};
        }

        if($keyword == ''){
            if($reverse == 'revers'){
                $finder = User::with('comments')
                    ->get()
                    ->sortBy($value)->reverse();
            }elseif($reverse == 'n_revers'){
                $finder = User::with('comments')
                    ->get()
                    ->sortBy($value);
            }else{
                $finder = User::with('comments')
                    ->get();
            }
        }else{
            if($reverse == 'revers'){
                $finder = User::with('comments')
                    ->where('name', 'LIKE', "{$keyword}%")
                    ->orWhere('surname', 'LIKE', "%{$keyword}%")
                    ->orWhere('phone', 'LIKE', "%{$keyword}%")
                    ->orWhere('address', 'LIKE', "%{$keyword}%")
                    ->get()
                    ->sortBy($value)->reverse();
            }elseif($reverse == 'n_revers'){
                $finder = User::with('comments')
                    ->where('name', 'LIKE', "{$keyword}%")
                    ->orWhere('surname', 'LIKE', "%{$keyword}%")
                    ->orWhere('phone', 'LIKE', "%{$keyword}%")
                    ->orWhere('address', 'LIKE', "%{$keyword}%")
                    ->get()
                    ->sortBy($value);
            }else{
                $finder = User::with('comments')
                    ->where('name', 'LIKE', "{$keyword}%")
                    ->orWhere('surname', 'LIKE', "%{$keyword}%")
                    ->orWhere('phone', 'LIKE', "%{$keyword}%")
                    ->orWhere('address', 'LIKE', "%{$keyword}%")
                    ->get()
                    ->sortBy($sort);
            }
        }

        $data = [];

        foreach ($finder as $find){
            $user_last_comment = User::getLastComment($find->id);
            $user_count_comment = User::getCountComments($find->id);
            $data[] = [
                'name'              => $find->name,
                'surname'           => $find->surname,
                'created_at'        => $find->created_at,
                'phone'             => $find->phone,
                'address'           => $find->address,
                'last_comment'      => $user_last_comment->text,
                'likes'             => $user_count_comment
            ];
        }

        return $data;
    }

}
