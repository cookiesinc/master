<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class CommentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        foreach(range(1, 60) as $index) {
            \App\Comment::create([
                'user_id'       => rand(1,10),
                'author'        => $faker->name,
                'text'          => $faker->paragraph(4),
                'rating'        => rand(1,5)
            ]);
        }
    }
}
