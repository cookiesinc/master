
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/favicon.ico">

    <title>{{ config('app.name') }}</title>

    <!-- Bootstrap core CSS -->
    <link href="//getbootstrap.com/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="/css/app.css" rel="stylesheet">


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">{{ config('app.name') }}</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <form class="navbar-form navbar-right" method="get" action="">
                <div class="form-group">
                    <input id="search" type="text" placeholder="Email" class="form-control">
                </div>
            </form>
            <button id="order_data" class="btn btn-success" data-type="asc">Data: ASD/DESK</button>
            <button id="order_like" class="btn btn-success" data-type="revers">Like: ASD/DESK</button>
        </div><!--/.navbar-collapse -->
    </div>
</nav>

<div class="container">
    <!-- Example row of columns -->
    <div class="row" id="content">

        @foreach($users as $user)
            <div id="user_block" class="col-md-4">
                <h2>{{ $user->name }}&nbsp;{{ $user->surname }}</h2>
                <p>{{ $user->created_at }}</p>
                <p>{{ $user->phone }}</p>
                <p class="address">{{ $user->address }}</p>
                <p class="comment">{{ User::getLastComment($user->id)->text }}</p>
                <p>{{ User::getCountComments($user->id) }} Likes</p>
            </div>
        @endforeach

    </div>

</div> <!-- /container -->


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="//getbootstrap.com/dist/js/bootstrap.min.js"></script>

<script>
    $(function() {

        function tobase(sort,reverse,me_id)
        {
            var keyword = $("#search").val();
            if(keyword=='') {
                keyword = '';
            }

            $.ajax({
                url: "{{ route('search') }}",
                dataType: "JSON",
                contentType: "application/json",
                data: {
                    'keyword': keyword,
                    'sort': sort,
                    'reverse': reverse,
                    '_token': '{{ csrf_token() }}'
                },
                success: function(response){
                    if(reverse == 'n_revers'){
                        $('#'+me_id+'').attr('data-type', 'revers');
                    }else{
                        $('#'+me_id+'').attr('data-type', 'n_revers');
                    }

                    document.getElementById("content").innerHTML = '';
                    response.forEach(function(data) {
                        $("#content").append(

                                '<div id="user_block" class="col-md-4">' +
                                '<h2>'+  data.name + ' ' + data.surname + '</h2>' +
                                '<p>' + data.created_at.date + '</p>' +
                                '<p>' + data.phone + '</p>' +
                                '<p class="address">' + data.address + '</p>' +
                                '<p class="comment">' + data.last_comment + '</p>' +
                                '<p>' + data.likes + ' Likes</p>' +
                                '</div>'

                        );
                    });
                }
            });
        }

        $("#search").keyup(function() {
            var sort = 'name';
            var reverse = 'desc';
            tobase(sort,reverse);
        });

        $('#order_data').on('click',function () {
            var sort = 'created_at';
            var reverse = $('#order_data').attr('data-type');
            var me_id = 'order_data';
            tobase(sort,reverse,me_id);
        });

        $('#order_like').on('click',function () {
            var sort = 'rating';
            var reverse = $('#order_like').attr('data-type');
            var me_id = 'order_like';
            tobase(sort,reverse,me_id);
        });


    });
</script>
</body>
</html>
