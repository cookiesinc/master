<?php
//Main page
Route::get('/', ['as' => 'main', 'uses' => 'FrontController@index']);

Route::get('/search', ['as' => 'search', 'uses' => 'FrontController@search']);
//Route::get('/order_data', ['as' => 'order_data', 'uses' => 'FrontController@order_data']);
